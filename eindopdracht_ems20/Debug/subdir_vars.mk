################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../CC3220S_LAUNCHXL_TIRTOS.cmd 

C_SRCS += \
../CC3220S_LAUNCHXL.c \
../client_cbs.c \
../main_tirtos.c \
../mqtt_client_app.c \
../network_if.c \
../ti_drivers_net_wifi_config.c \
../uart_term.c 

C_DEPS += \
./CC3220S_LAUNCHXL.d \
./client_cbs.d \
./main_tirtos.d \
./mqtt_client_app.d \
./network_if.d \
./ti_drivers_net_wifi_config.d \
./uart_term.d 

OBJS += \
./CC3220S_LAUNCHXL.obj \
./client_cbs.obj \
./main_tirtos.obj \
./mqtt_client_app.obj \
./network_if.obj \
./ti_drivers_net_wifi_config.obj \
./uart_term.obj 

OBJS__QUOTED += \
"CC3220S_LAUNCHXL.obj" \
"client_cbs.obj" \
"main_tirtos.obj" \
"mqtt_client_app.obj" \
"network_if.obj" \
"ti_drivers_net_wifi_config.obj" \
"uart_term.obj" 

C_DEPS__QUOTED += \
"CC3220S_LAUNCHXL.d" \
"client_cbs.d" \
"main_tirtos.d" \
"mqtt_client_app.d" \
"network_if.d" \
"ti_drivers_net_wifi_config.d" \
"uart_term.d" 

C_SRCS__QUOTED += \
"../CC3220S_LAUNCHXL.c" \
"../client_cbs.c" \
"../main_tirtos.c" \
"../mqtt_client_app.c" \
"../network_if.c" \
"../ti_drivers_net_wifi_config.c" \
"../uart_term.c" 


