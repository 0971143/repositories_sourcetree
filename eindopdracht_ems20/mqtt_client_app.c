/*
 * Copyright (c) 2016, Texas Instruments Incorporated
 * All rights reserved.
 * Simplified (c) 2019, Hogeschool Rotterdam
 */

/*****************************************************************************

   Application Name     -  MQTT Client
   Application Overview -  The device is running a MQTT client which is
                           connected to the online broker. Three LEDs on the
                           device can be controlled from a web client by
                           publishing msg on appropriate topics. Similarly,
                           a message can be published on a pre-configured topic
                           by pressing the switch SW2 button on the device.

*****************************************************************************/

#include <stdlib.h>
#include <pthread.h>
#include <mqueue.h>
#include <time.h>
#include <unistd.h>
#include <stdio.h>

#include <ti/drivers/GPIO.h>
#include <ti/drivers/SPI.h>

#include <ti/drivers/net/wifi/simplelink.h>
#include <ti/drivers/net/wifi/slnetifwifi.h>

#include <ti/net/mqtt/mqttclient.h>

#include "network_if.h"
#include "uart_term.h"

#include "Board.h"
#include "client_cbs.h"
#include <ti/posix/ccs/pthread.h>
#include <ti/posix/ccs/semaphore.h>

#define CLIENT_INIT_STATE (0x01)
#define MQTT_INIT_STATE (0x04)

/* Operate library in MQTT 3.1 mode. */
#define MQTT_3_1 true

/* Defining broker IP address and port number */
#define SERVER_ADDRESS "broker.hivemq.com"
#define PORT_NUMBER 1883

/* Defining subscription topic values */
#define SUBSCRIPTION_TOPIC0 "/ems20/0971143/initdatetime"
#define SUBSCRIPTION_TOPIC1 "/ems20/0971143/getdatetime"
#define SUBSCRIPTION_TOPIC2 "nietBelangrijk"

/* Defining publish topic values */
#define PUBLISH_TOPIC0 "/ems20/0971143/datetime"
sem_t wachtenOpIP;
void Mqtt_ClientStop(uint8_t disconnect);
int32_t MqttClient_start();

void *App_Task(void *args);

/* Global variables */
typedef struct{
    UART_Handle uartje;

    mqd_t postbus;
}moker;

bool gResetApplication = false;
/* Connection state: (0) - connected, (negative) - disconnected */
static int32_t gApConnectionState = -1;
static uint32_t gInitState = 0;
static uint32_t gUiConnFlag = 0;
static MQTTClient_Handle gMqttClient;

/* If ClientId isn't set, the MAC address of the device will be copied into */
/* the ClientID variable. */
char ClientId[13] = {'\0'};

/* Message Queue */
mqd_t g_PBQueue;

//*
// MQTT_SendMsgToQueue - Utility function that sends msgType parameter to
// the message queue g_PBQueue with timeout of 0.
// If the queue isn't full the parameter will be stored and the function
// will return true.
// If the queue is full and the timeout expired (because the timeout parameter
// is 0 it will expire immediately), the parameter is thrown away and the
// function will return false.
//*
bool MQTT_SendMsgToQueue(msgType *msg)
{
    struct timespec abstime = {0};

    clock_gettime(CLOCK_REALTIME, &abstime);

    if (g_PBQueue)
    {
        /* send message to the message queue */
        if (mq_timedsend(g_PBQueue, (char *) msg, sizeof(msg), 0, &abstime) == 0)
        {
            return true;
        }
        UART_PRINT("Message queue is full.\n\r");
        return false;
    }
    UART_PRINT("Message queue does not exist.\n\r");
    return false;
}

//*
// Push Button Handler1(GPIOSW2). Press push button0 (GPIOSW2) whenever user
// wants to publish a message. Write message into message queue signaling the
// event publish messages
//*
void pushButtonInterruptHandler2(uint_least8_t index)
{
    msgType msg;
    msg.event = PUBLISH_PUSH_BUTTON_PRESSED;

    MQTT_SendMsgToQueue(&msg);
}

//*
// Push Button Handler2(GPIOSW3). Press push button3 whenever you want to
// disconnect from the remote broker.
//*
void pushButtonInterruptHandler3(uint_least8_t index)
{
    gResetApplication = true;
}

void *MqttClientThread(void *pvParameters)
{
    MQTTClient_run(gMqttClient);

    msgType msg;
    msg.event = LOCAL_CLIENT_DISCONNECTION;

    if (MQTT_SendMsgToQueue(&msg) == false)
    {
        UART_PRINT("Throw away first message and send the new one.\n\r");
        msgType msgRecv;
        mq_receive(g_PBQueue, (char *)&msgRecv, sizeof(msgRecv), NULL);
        MQTT_SendMsgToQueue(&msg);
    }

    pthread_exit(0);

    return NULL;
}

//*
// Task implementing MQTT Server plus client bridge
//
// This function
//    1. Initializes network driver and connects to the default AP
//    2. Initializes the mqtt client librarie and set up MQTT
//       with the remote broker.
//    3. set up the button event and its callback (for publishing)
//    4. handles the callback signals
//
//*
void *MqttClient(void *pvParameters)
{
    moker app_post = *(moker *)pvParameters;
    char opvragen[14];
    opvragen[0] = 'N';

    /* Initializing client and subscribing to the broker. */
    if (gApConnectionState >= 0)
    {
        int32_t retVal = MqttClient_start();
        if (retVal == -1)
        {
            UART_PRINT("MQTT Client library initialization failed.\n\r");
            pthread_exit(0);
            return NULL;
        }
    }

    /* Handling the signals from various callbacks */
    while (1)
    {
        /* waiting for the message queue */
        msgType msgRecv;
        mq_receive(g_PBQueue, (char *)&msgRecv, sizeof(msgRecv), NULL);

        switch(msgRecv.event)
        {
            case PUBLISH_PUSH_BUTTON_PRESSED:
            {

                static int number_of_pushes = 0;
                number_of_pushes++;

                char publish_data[14];
                sprintf(publish_data, "%d", number_of_pushes);

                int16_t retVal = MQTTClient_publish(gMqttClient, PUBLISH_TOPIC0, strlen(PUBLISH_TOPIC0),
                    publish_data, strlen(publish_data), MQTT_QOS_2);

                if (retVal < 0)
                {
                    UART_PRINT("MQTT publish failed.\n\r");
                }
                else
                {
                    UART_PRINT("\n\rCC3200 publishes the following message:\n\r");
                    UART_PRINT("Topic: %s\n\r", PUBLISH_TOPIC0);
                    UART_PRINT("Data: %s\n\r", publish_data);
                }
                break;
            }
            case MSG_RECV_BY_CLIENT:
                if (strcmp(msgRecv.topic, SUBSCRIPTION_TOPIC0) == 0)
                {

                    mq_send(app_post.postbus, (char *)&msgRecv.payload, sizeof(msgRecv.payload), NULL);
                }
                else if (strcmp(msgRecv.topic, SUBSCRIPTION_TOPIC1) == 0)

                {

                    mq_send(app_post.postbus, (char *)&opvragen, sizeof(opvragen), NULL);
                    //als we de array "opvragen" naar app_task sturen, zullen we kunnen lezen
                    //dat er op de "GET" knop is gedrukt door een N op de eerste index neer te zetten

                }
                else if (strcmp(msgRecv.topic, SUBSCRIPTION_TOPIC2) == 0)
                {
                    mq_send(app_post.postbus, (char *)&msgRecv.payload, sizeof(msgRecv.payload), NULL);
                }
                break;

            /* On-board client disconnected from remote broker */
            case LOCAL_CLIENT_DISCONNECTION:
                UART_PRINT("\n\rOn-board client disconnected.\r\n");
                gUiConnFlag = 0;
                break;

            case THREAD_TERMINATE_REQ:
                gUiConnFlag = 0;
                pthread_exit(0);
                return NULL;
        }
    }
}

//*
// This function connect the MQTT device to an AP with the SSID which was
// configured in SSID_NAME definition which can be found in Network_if.h file,
// if the device can't connect to this AP a request from the user for other
// (open) SSID will appear.
//*
int32_t Mqtt_IF_Connect()
{
    /* Reset The state of the machine */
    Network_IF_ResetMCUStateMachine();

    /* Start the driver */
    long lRetVal = Network_IF_InitDriver(ROLE_STA);
    if (lRetVal < 0)
    {
        UART_PRINT("Failed to start SimpleLink Device. Error: %d.\n\r", lRetVal);
        return -1;
    }
    else
    {
        UART_PRINT("SimpleLink Device started successfully.\n\r");
    }

    /* Initialize AP security params */
    SlWlanSecParams_t SecurityParams = { 0 };
    SecurityParams.Key = SECURITY_KEY;
    SecurityParams.KeyLen = strlen(SECURITY_KEY);
    SecurityParams.Type = SECURITY_TYPE;

    /* Connect to the Access Point */
    lRetVal = Network_IF_ConnectAP(SSID_NAME, SecurityParams);
    if (lRetVal < 0)
    {
        UART_PRINT("Connection to AP %s failed\n\r", SSID_NAME);
        return -1;
    }
    else
    {
        UART_PRINT("SimpleLink Device connected to AP %s successfully.\n\r", SSID_NAME);
    }

    return 0;
}

//*
// MQTT Start - Initialize and create all the items required to run the MQTT
// protocol
//*
void Mqtt_start()
{
    mqd_t postbusje;
    mq_attr postbusjeattr;
    postbusjeattr.mq_flags = 0;
    postbusjeattr.mq_maxmsg = 2;
    postbusjeattr.mq_msgsize = 14;
    postbusjeattr.mq_curmsgs = 2;

    postbusje = mq_open("postbusje", O_RDWR | O_CREAT, 0666, &postbusjeattr);

    mq_attr attr;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = sizeof(msgType);
    g_PBQueue = mq_open("g_PBQueue", O_CREAT|O_RDWR, 0, &attr);

    if (g_PBQueue == (mqd_t)-1)
    {
        UART_PRINT("MQTT Message Queue create failed.\n\r");
        gInitState &= ~MQTT_INIT_STATE;
        return;
    }

    /* Create mqttThread */
    pthread_attr_t pAttrs;
    pthread_attr_init(&pAttrs);
    struct sched_param priParam;
    priParam.sched_priority = 2;
    int retc = pthread_attr_setschedparam(&pAttrs, &priParam);
    retc |= pthread_attr_setstacksize(&pAttrs, 2048);
    retc |= pthread_attr_setdetachstate(&pAttrs, PTHREAD_CREATE_DETACHED);
    pthread_t mqttThread, app_thread;
    moker p1 = {NULL, postbusje};
    retc |= pthread_create(&mqttThread, &pAttrs, MqttClient, &p1);
    retc |= pthread_create(&app_thread, &pAttrs, App_Task, &p1);
    if(retc != 0)
    {
        gInitState &= ~MQTT_INIT_STATE;
        UART_PRINT("MQTT thread create failed.\n\r");
        return;
    }

    /* Enable interrupt GPIO 22 (SW2) */
    GPIO_setCallback(Board_GPIO_BUTTON0, pushButtonInterruptHandler2);
    GPIO_enableInt(Board_GPIO_BUTTON0);
    /* Enable interrupt GPIO 13 (SW3) */
    GPIO_setCallback(Board_GPIO_BUTTON1, pushButtonInterruptHandler3);
    GPIO_enableInt(Board_GPIO_BUTTON1);

    gInitState &= ~MQTT_INIT_STATE;
}

//*
// MQTT Stop - Close the client instance and free all the items required to
// run the MQTT protocol
//*
void Mqtt_Stop()
{
    if (gApConnectionState >= 0)
    {
        Mqtt_ClientStop(1);
    }

    msgType msg;
    msg.event = THREAD_TERMINATE_REQ;

    if (MQTT_SendMsgToQueue(&msg) == false)
    {
        UART_PRINT("Throw away first message and send the new one.\n\r");
        msgType msgRecv;
        mq_receive(g_PBQueue, (char *)&msgRecv, sizeof(msgRecv), NULL);
        MQTT_SendMsgToQueue(&msg);
    }

    sleep(2);

    mq_close(g_PBQueue);
    g_PBQueue = NULL;

    sl_Stop(SL_STOP_TIMEOUT);
    UART_PRINT("Client stop completed.\r\n");

    /* Disable the SW2 interrupt */
    GPIO_disableInt(Board_GPIO_BUTTON0);
    /* Disable the SW3 interrupt */
    GPIO_disableInt(Board_GPIO_BUTTON1);
}

int32_t MqttClient_start()
{
    /* Open client receive thread and start the receive task. */

    MQTTClient_ConnParams Mqtt_ClientCtx =
    {
        MQTTCLIENT_NETCONN_URL, SERVER_ADDRESS, PORT_NUMBER, 0, 0, 0, NULL
    };

    MQTTClient_Params MqttClientExmple_params;
    MqttClientExmple_params.clientId = ClientId;
    MqttClientExmple_params.connParams = &Mqtt_ClientCtx;
    MqttClientExmple_params.mqttMode31 = MQTT_3_1;
    MqttClientExmple_params.blockingSend = true;

    gInitState |= CLIENT_INIT_STATE;

    /*Initialize MQTT client library */
    gMqttClient = MQTTClient_create(MqttClientCallback, &MqttClientExmple_params);

    if (gMqttClient == NULL)
    {
        /* lib initialization failed                                         */
        gInitState &= ~CLIENT_INIT_STATE;
        return -1;
    }

    pthread_attr_t pAttrs;
    pthread_attr_init(&pAttrs);
    struct sched_param priParam;
    priParam.sched_priority = 2;
    int retVal = pthread_attr_setschedparam(&pAttrs, &priParam);
    retVal |= pthread_attr_setstacksize(&pAttrs, 4096);
    retVal |= pthread_attr_setdetachstate(&pAttrs, PTHREAD_CREATE_DETACHED);
    pthread_t rx_task;
    retVal |= pthread_create(&rx_task, &pAttrs, MqttClientThread, NULL);
    if (retVal != 0)
    {
        UART_PRINT("Client thread create failed.\n\r");
        gInitState &= ~CLIENT_INIT_STATE;
        return -1;
    }

    /* Initiate MQTT connect */
    if (gApConnectionState >= 0)
    {
        /* The return code of MQTTClient_connect is the ConnACK value that
         *  returns from the server */
        int16_t retVal = MQTTClient_connect(gMqttClient);

        /* Negative retVal means error, 0 means connection successful without
         * session stored by the server, greater than 0 means successful
         * connection with session stored by the server */
        if (retVal < 0)
        {
            /* Library initialization failed                                     */
            UART_PRINT("Connection to broker failed, Error code: %d.\n\r", retVal);
            gUiConnFlag = 0;
        }
        else
        {
            gUiConnFlag = 1;
        }
        /* Subscribe to topics when session is not stored by the server */
        if ((gUiConnFlag == 1) && (retVal == 0))
        {
            MQTTClient_SubscribeParams subscriptionInfo[] = {
                {SUBSCRIPTION_TOPIC0, NULL, MQTT_QOS_2, 0},
                {SUBSCRIPTION_TOPIC1, NULL, MQTT_QOS_2, 0},
                {SUBSCRIPTION_TOPIC2, NULL, MQTT_QOS_2, 0}
            };

            if (MQTTClient_subscribe(gMqttClient, subscriptionInfo,
                sizeof(subscriptionInfo)/sizeof(subscriptionInfo[0])) < 0)
            {
                UART_PRINT("Subscribe error.\n\r");
                MQTTClient_disconnect(gMqttClient);
                gUiConnFlag = 0;
            }
            else
            {
                uint8_t i;
                for (i = 0; i < sizeof(subscriptionInfo)/sizeof(subscriptionInfo[0]); i++)
                {
                    UART_PRINT("Client subscribed on %s\n\r", subscriptionInfo[i].topic);
                }
            }
        }
    }

    gInitState &= ~CLIENT_INIT_STATE;

    return 0;
}

void Mqtt_ClientStop(uint8_t disconnect)
{
    MQTTClient_UnsubscribeParams unsubscriptionInfo[] = {
        {SUBSCRIPTION_TOPIC0, 0},
        {SUBSCRIPTION_TOPIC1, 0},
        {SUBSCRIPTION_TOPIC2, 0}
    };

    if (MQTTClient_unsubscribe(gMqttClient, unsubscriptionInfo,
        sizeof(unsubscriptionInfo)/sizeof(unsubscriptionInfo[0])) < 0)
    {
        UART_PRINT("Unsubscribe error.\n\r");
        MQTTClient_disconnect(gMqttClient);
        gUiConnFlag = 0;
    }
    else
    {
        uint8_t i;
        for (i = 0; i < sizeof(unsubscriptionInfo)/sizeof(unsubscriptionInfo[0]); i++)
        {
            UART_PRINT("Client unsubscribed from %s\n\r", unsubscriptionInfo[i].topic);
        }
    }

    gUiConnFlag = 0;

    /*exiting the Client library */
    MQTTClient_delete(gMqttClient);
}

//*
// Set the ClientId with its own mac address
// This routine converts the mac address which is given
// by an integer type variable in hexadecimal base to ASCII
// representation, and copies it into the ClientId parameter.
//*
int32_t SetClientIdNamefromMacAddress()
{
    int32_t ret;
    uint8_t Client_Mac_Name[2];

    /* When ClientID isn't set, use the MAC address as ClientID */
    if(ClientId[0] == '\0')
    {
        /* Get the device MAC address */
        uint16_t macAddressLen = SL_MAC_ADDR_LEN;
        uint8_t macAddress[SL_MAC_ADDR_LEN];
        ret = sl_NetCfgGet(SL_NETCFG_MAC_ADDRESS_GET, 0, &macAddressLen, macAddress);

        uint8_t Index;
        for (Index = 0; Index < SL_MAC_ADDR_LEN; Index++)
        {
            /* Each MAC address byte contains two hexadecimal characters */
            /* Copy the 4 MSB - the most significant character */
            Client_Mac_Name[0] = (macAddress[Index] >> 4) & 0xf;
            /* Copy the 4 LSB - the least significant character */
            Client_Mac_Name[1] = macAddress[Index] & 0xf;

            uint8_t i;
            for (i = 0; i <= 1; i++)
            {
                if (Client_Mac_Name[i] > 9)
                {
                    /* Converts and copies from number that is greater than 9 in
                     * hexadecimal representation (a to f) into ASCII character */
                    ClientId[2 * Index + i] = Client_Mac_Name[i] + 'a' - 10;
                }
                else
                {
                    /* Converts and copies from number 0 - 9 in hexadecimal
                     * representation into ASCII character */
                    ClientId[2 * Index + i] = Client_Mac_Name[i] + '0';
                }
            }
        }
    }
    return ret;
}
//struct voor de threads. bevat een uart handle om de uart in te stellen en een postbus om de tijd en datum door te geven

void *App_Task(void *args)
{

    moker meegegevenZooi = *(moker *)args;

    _i8 buffer1[14];


    _i16 accepteren;
    _i16 foutHerkenning;
    meegegevenZooi.uartje;
    SlNetCfgIpV4Args_t ipV4 = {0}; // Struct voor ip

    SlSockAddrIn_t schildpad;
    SlSockAddrIn_t  Giovanni_socket;
    _i16 AddrSize = sizeof(SlSockAddrIn_t);
    UART_PRINT("TEST");

    foutHerkenning = sl_Socket(SL_AF_INET, SL_SOCK_STREAM, SL_IPPROTO_TCP);
    if(foutHerkenning < 0){
        while(1){

        }
    }
    schildpad.sin_family =  SL_AF_INET;
    schildpad.sin_port = sl_Htons(2000);
    schildpad.sin_addr.s_addr = SL_INADDR_ANY;
    schildpad.sin_zero;

    _i16 ligma = sl_Bind(foutHerkenning, (SlSockAddr_t *)&schildpad, sizeof(SlSockAddrIn_t));
    _i16 joe = sl_Listen(foutHerkenning, 1);
    UART_PRINT( "\r\nwachten");
    accepteren = sl_Accept(foutHerkenning, NULL, 0);
    UART_PRINT( "\r\nconnectie");

    mq_receive(meegegevenZooi.postbus, (char*)&buffer1, sizeof(buffer1), 0);

    if (buffer1[0] = 'N'){   //als de eerste index een N is, is er op de "GET" knop gedrukt en dus moeten we alleen dan publishen.
        _i16 yeet = sl_Send(accepteren, buffer1, 14, 0);
        _i16 ontvangen = sl_Recv(accepteren, buffer1, 14, 0);
        int16_t retVal = MQTTClient_publish(gMqttClient, PUBLISH_TOPIC0, strlen(PUBLISH_TOPIC0),
                                        buffer1, strlen(buffer1), MQTT_QOS_2);
    }
    else if(buffer1[0] = '2'){
        _i16 yeet = sl_Send(accepteren, buffer1, 14, 0);
    }


    sl_Close(foutHerkenning);



    return NULL;
}

void *mainThread(void *args)
{

    /* Initialize SlNetSock layer with CC32xx interface */
    SlNetIf_init(0);
    SlNetIf_add(SLNETIF_ID_1, "CC32xx", (const SlNetIf_Config_t *)&SlNetIfConfigWifi, 5);

    SlNetSock_init(0);
    SlNetUtil_init(0);

    GPIO_init();
    SPI_init();
    sem_init(&wachtenOpIP, 0, 0);


    GPIO_write(Board_GPIO_LED0, Board_GPIO_LED_OFF);
    GPIO_write(Board_GPIO_LED1, Board_GPIO_LED_OFF);
    GPIO_write(Board_GPIO_LED2, Board_GPIO_LED_OFF);

    /* Configure the UART */
    UART_Handle tUartHndl = InitTerm();
    /* Remove uart receive from LPDS dependency */
    UART_control(tUartHndl, UART_CMD_RXDISABLE, NULL);

    /*Create the sl_Task */
    pthread_attr_t pAttrs_spawn;
    pthread_attr_init(&pAttrs_spawn);
    struct sched_param priParam;
    priParam.sched_priority = 9;
    int retc = pthread_attr_setschedparam(&pAttrs_spawn, &priParam);
    retc |= pthread_attr_setstacksize(&pAttrs_spawn, 2048);
    retc |= pthread_attr_setdetachstate(&pAttrs_spawn, PTHREAD_CREATE_DETACHED);
    pthread_t spawn_thread;
//    moker p1 = {tUartHndl, postbusje};
    retc |= pthread_create(&spawn_thread, &pAttrs_spawn, sl_Task, NULL);
//    retc |= pthread_create(&app_thread, &pAttrs_spawn, App_Task, &p1);
    if (retc != 0)
    {
        UART_PRINT("Could not create simplelink task.\r\n");
        while (1);
    }
//    priParam.sched_priority = 2;

    retc = sl_Start(0, 0, 0);
    if (retc < 0)
    {
        UART_PRINT("sl_Start failed.\r\n");
        while (1);
    }

    /* Output device information to the UART terminal */
    /* Set the ClientId with its own mac address */
    retc = SetClientIdNamefromMacAddress();

    retc |= sl_Stop(SL_STOP_TIMEOUT);
    if (retc < 0)
    {
        UART_PRINT("sl_Stop failed\r\n");
        while (1);
    }

    uint32_t count = 0;
    while (1)
    {
        gResetApplication = false;
        gInitState = 0;

        /* Connect to AP */
        gApConnectionState = Mqtt_IF_Connect();

        gInitState |= MQTT_INIT_STATE;
        /* Run MQTT main thread (it will open the client) */
        Mqtt_start();

        /* Wait for init to be completed */
        while (gInitState != 0)
        {
            UART_PRINT(".");
            sleep(1);
        }


        while (gResetApplication == false);

        UART_PRINT("TO Complete - Closing all threads and resources\r\n");

        /*Stop the MQTT Process                                              */
        Mqtt_Stop();

        UART_PRINT("\r\nReopen MQTT # %d\r\n", ++count);
    }
}
